var tinhNgay = function () {
  var Thangvalue = document.getElementById("txt_thang").value * 1;
  var Namvalue = document.getElementById("txt_nam").value * 1;
  var thang31Ngay =
    Thangvalue == 1 ||
    Thangvalue == 3 ||
    Thangvalue == 5 ||
    Thangvalue == 7 ||
    Thangvalue == 9 ||
    Thangvalue == 11;
  var thang30Ngay =
    Thangvalue == 4 ||
    Thangvalue == 6 ||
    Thangvalue == 8 ||
    Thangvalue == 10 ||
    Thangvalue == 12;
  var thang28Ngay = Thangvalue == 2;
  var namNhuan = Namvalue % 400 == 0;
  if (thang31Ngay) {
    document.getElementById(
      "ket_qua"
    ).innerHTML = `Tháng ${Thangvalue} năm ${Namvalue} có 31 ngày`;
  } else if (thang30Ngay && !namNhuan) {
    document.getElementById(
      "ket_qua"
    ).innerHTML = `Tháng ${Thangvalue} năm ${Namvalue} có 30 ngày`;
  } else if (thang28Ngay && namNhuan) {
    document.getElementById(
      "ket_qua"
    ).innerHTML = `Tháng ${Thangvalue} năm ${Namvalue} có 29 ngày`;
  } else if (thang28Ngay) {
    document.getElementById(
      "ket_qua"
    ).innerHTML = `Tháng ${Thangvalue} năm ${Namvalue} có 28 ngày`;
  } else {
    document.getElementById("ket_qua").innerHTML = `Tháng 0 năm 0 có 0 ngày`;
  }
};
