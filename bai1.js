var ngayHomQua = function () {
  var Ngayvale = document.getElementById("txt_ngay").value * 1;
  var Thangyvale = document.getElementById("txt_thang").value * 1;
  var Namvale = document.getElementById("txt_nam").value * 1;
  var ngayhomqua = Ngayvale - 1;
  if (
    Ngayvale <= 0 ||
    Ngayvale > 31 ||
    Thangyvale <= 0 ||
    Thangyvale > 12 ||
    Namvale <= 0
  ) {
    document.getElementById("ket_qua").innerHTML = `<p>Ngày không xác định</p>`;
  } else if (Ngayvale == 1 && Thangyvale != 1) {
    Ngayvale += 30;
    Thangyvale = Thangyvale - 1;
    document.getElementById(
      "ket_qua"
    ).innerHTML = `${Ngayvale}/${Thangyvale}/${Namvale}`;
  } else if (Ngayvale == 1 && Thangyvale == 1) {
    Ngayvale += 30;
    Thangyvale += 11;
    Namvale = Namvale - 1;
    document.getElementById(
      "ket_qua"
    ).innerHTML = `${Ngayvale}/${Thangyvale}/${Namvale}`;
  } else {
    document.getElementById(
      "ket_qua"
    ).innerHTML = `${ngayhomqua}/${Thangyvale}/${Namvale}`;
  }
};

var ngayMai = function () {
  var Ngayvale = document.getElementById("txt_ngay").value * 1;
  var Thangyvale = document.getElementById("txt_thang").value * 1;
  var Namvale = document.getElementById("txt_nam").value * 1;
  var ngaymai = Ngayvale + 1;
  if (
    Ngayvale <= 0 ||
    Ngayvale > 31 ||
    Thangyvale <= 0 ||
    Thangyvale > 12 ||
    Namvale <= 0
  ) {
    document.getElementById("ket_qua").innerHTML = `<p>Ngày không xác định</p>`;
  } else if (Ngayvale == 31 && Thangyvale != 12) {
    Ngayvale = Ngayvale - 30;
    Thangyvale += 1;
    document.getElementById(
      "ket_qua"
    ).innerHTML = `${Ngayvale}/${Thangyvale}/${Namvale}`;
  } else if (Ngayvale == 31 && Thangyvale == 12) {
    Ngayvale = Ngayvale - 30;
    Thangyvale = Thangyvale - 11;
    Namvale = Namvale + 1;
    document.getElementById(
      "ket_qua"
    ).innerHTML = `${Ngayvale}/${Thangyvale}/${Namvale}`;
  } else {
    document.getElementById(
      "ket_qua"
    ).innerHTML = `${ngaymai}/${Thangyvale}/${Namvale}`;
  }
};
